const fs = require('fs');

i18nPath = 'src/assets/i18n';
enPath = i18nPath + '/en_US/';
fs.watch(enPath, function (event, filename) {
    console.log('event is: ' + event);
    if (filename) {
        console.log('filename provided: ' + filename);
        fs.createReadStream(enPath + filename).pipe(fs.createWriteStream(i18nPath + '/en.json'));
    } else {
        console.log('filename not provided');
    }
});

vnPath = i18nPath + '/vi_VN/';
fs.watch(enPath, function (event, filename) {
    console.log('event is: ' + event);
    if (filename) {
        console.log('filename provided: ' + filename);
        fs.createReadStream(vnPath + filename).pipe(fs.createWriteStream(i18nPath + '/vi.json'));
    } else {
        console.log('filename not provided');
    }
});
