**Edit build.gradle**

Add to classpath:

_classpath 'com.google.gms:google-services:3.1.0'_

Add under dependencies:

_apply plugin: 'com.google.gms.google-services'_


Add permission manifest:


    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />


- Remove cordova-plugin-keyboard from android platform. (duplicate)

- Add NSLocationWhenInUseUsageDescription to *-Info.plist with value: "need location access to find things nearby"



** To use Local Notification **
required gradle 4.4
```export CORDOVA_ANDROID_GRADLE_DISTRIBUTION_URL=https\://services.gradle.org/distributions/gradle-4.4-all.zip```


fcm 2.1.2
Change string.xml path to platforms/android/app/src/main/res/values/strings.xml in file plugins/cordova-plugin-fcm/scripts/fcm_config_files_process.js
