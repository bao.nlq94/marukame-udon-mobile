export class Branch {
    bid: number;
    CloseTime: string;
    OpenTime: string;
    babout_url: string;
    badd: string;
    badd_full: string;
    bdes: string;
    bdes_full: string;
    bimg_url: string;
    blat: number;
    blng: number;
    bname: string;
    bname_full: string;
    bphone: string;
    bphone_full: string;
    bzone: string;
}
