export interface BaseResponse {
    resultCode: string;
    result: any;
    msg: string;
}
