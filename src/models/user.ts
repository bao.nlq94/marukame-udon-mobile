export class User {
    constructor(){
        
    }
    id: number;
    fullname: string;
    email: string;
    accessToken: string;
    linkedAccountId: string;
    linkedType: string;
    phone:string;
    gender:number;
    dateOfBirth:string;
    address:string;
    cardno:string;
}
