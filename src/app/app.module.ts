import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { QRCodeModule } from 'angularx-qrcode';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AuthProvider } from '../providers/auth/auth';
import { RestfulProvider } from '../providers/restful';
import { BranchProvider } from '../providers/branch/branch';
import { LocationProvider } from '../providers/location/location';
import { Geolocation } from '@ionic-native/geolocation';
import { PromotionProvider } from '../providers/promotion/promotion';
import { UserProvider } from '../providers/user/user';
import { ToastmessageComponent } from '../components/toastmessage/toastmessage';
import { FavoriteProvider } from '../providers/favorite/favorite';
import { ProductionProvider } from '../providers/production/production';

import { GoogleAnalytics } from '@ionic-native/google-analytics';
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        BrowserModule,
        QRCodeModule,
        IonicModule.forRoot(MyApp, {
            backButtonText: '',
            iconMode: 'ios',
            tabsHideOnSubPages: false
        }),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        TranslateService,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        AuthProvider,
        RestfulProvider,
        BranchProvider,
        LocationProvider,
        LocationAccuracy,
        Geolocation,
        PromotionProvider,
        UserProvider, 
        ToastmessageComponent,
        FavoriteProvider,
        ProductionProvider,
        GoogleAnalytics
    ]
})
export class AppModule {
}
