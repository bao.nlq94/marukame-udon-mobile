import {HttpHeaders} from "@angular/common/http";
import { USER_TOKEN } from "../providers/auth/auth";

export class AppSettings {
    public static TOKEN_KEY: string = "currentUser";
    public static KEY_LANGUAGE: string = "language";
    public static VERSION_API: string = "1.0";
    public static SECRET_PWD_KEY: string = "mylife";

    public static getHeaders(): HttpHeaders {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json;charset=UTF-8',
        });
        return headers;
    }

    public static getAuthenticationHeaders(): HttpHeaders {
        const h = {
            'Content-Type': 'application/json;charset=UTF-8'
        }
        const token = localStorage.getItem(USER_TOKEN);
        if (token)
            h['Authorization'] = 'Bearer ' + token;

        let headers = new HttpHeaders(h);
        return headers;
    }

    public static getCurentUser(): any {
        try {
            return JSON.parse(localStorage.getItem(this.TOKEN_KEY));
        }
        catch (ex) {
            // console.log(ex);
            return null;
        }
    }

    public static getLanguage(): any {
        let language = localStorage.getItem(this.KEY_LANGUAGE);
        if (language === null)
            return 'vi';
        return language;
    }

}

export const TZ_HCM = "Asia/Ho_Chi_Minh";
export const GMT_7 = "+07:00";
export const API_ABS = 'http://quickapi.vnyi.com:81/';
// export const API_ABS = 'http://localhost:55922/';
export const API_ENDPOINT = API_ABS + 'api/v1.0';
export const GOOGLE_ANALYTICS_TRACKID="UA-77713356-3";

//Define ket Size pageable
export const SIZE_PAGEABLE = 10;

//Define key lang
export const ID_LANG_VI: string = "1";
export const ID_LANG_EN: string = "2";

export const Action = {
    OPEN_MENU: 1,
    OPEN_LOCATION: 2,
    OPEN_ABOUT: 3,
    OPEN_RESERVATION: 4
};
