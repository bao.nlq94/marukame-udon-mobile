import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from "@ngx-translate/core";
import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: NavController;
    rootPage: any = 'page-tabs';
    pages: Array<{ title: string, component: any }>;
    public selectedLang: string = 'vi';
    user: any;
    userDetail: any;
    cardScore: any;

    constructor(platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        private translate: TranslateService,
        auth: AuthProvider,
        private userProvider: UserProvider) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.pages = [
            { title: 'Danh sách nhà hàng', component: 'page-list-restaurant' },
            { title: 'Danh sách món ăn', component: 'page-home' },
            { title: 'Phản hồi và khiếu nại', component: 'page-home' }
        ];

        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');

        this.user = auth.getUser();
        if (this.user) {
            this.cardScore = {};
            this.getUserDetail();
            this.getCardScore();
        }

    }

    getUserDetail() {
        this.userProvider.getUserDetail(this.user.id).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.userDetail = res.result[0];
            }
        })
    }

    getCardScore() {
        this.userProvider.getCardScore(this.user.id).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.cardScore.curentName = res.currentLvl.name;
                this.cardScore.curentLevel = res.currentLvl.level;
                this.cardScore.curentPoint = res.currentLvl.point;
                this.cardScore.netxtLevel = res.nextLvl.level;
                this.cardScore.nextName = res.nextLvl.name;
                this.cardScore.nextPoint = res.nextLvl.point;
            }
        })
    }

    public changeLanguage(lang) {
        this.selectedLang = lang;
        this.translate.setDefaultLang(this.selectedLang);
    }

    openPage(p) {
        let childNav = this.nav.getActiveChildNav();
        const active = childNav.getSelected();
        console.log('activated nav ', active)
        active.push(p.component);
    }

    logout() {
        this.nav.setRoot('page-login');
    }
}
