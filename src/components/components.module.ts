import {NgModule} from '@angular/core';
import {IonRatingComponent} from './ion-rating/ion-rating';
import {RestaurantListItemComponent} from './restaurant-list-item/restaurant-list-item';
import {RestaurantCardItemComponent} from './restaurant-card-item/restaurant-card-item';
import {IonicPageModule} from "ionic-angular";
import {OpenCloseTimeComponent} from './open-close-time/open-close-time';
import {TranslateModule, TranslateService} from "@ngx-translate/core";



@NgModule({
    declarations: [
        IonRatingComponent,
        RestaurantListItemComponent,
        RestaurantCardItemComponent,
        OpenCloseTimeComponent,
    ],
    imports: [
        IonicPageModule.forChild(ComponentsModule),
        TranslateModule,
        
    ],
    exports: [
        IonRatingComponent,
        RestaurantListItemComponent,
        RestaurantCardItemComponent,
        OpenCloseTimeComponent,
    ],
    providers: [
        TranslateService,
    ]
})
export class ComponentsModule {
}
