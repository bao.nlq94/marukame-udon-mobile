import {ToastController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";
import {Injectable} from "@angular/core";

/**
 * Generated class for the ToastmessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Injectable()
export class ToastmessageComponent {
  constructor(public toastCtrl: ToastController,
    private translate: TranslateService) {
}

showToast(message: string) {
  this.translate.get(message).subscribe(
  value => {
      let toast = this.toastCtrl.create({
          message: value,
          cssClass: 'custom-toast',
          duration: 2000,
          position: 'top'
      });
      toast.present(toast);
  })
}

translateService(key): string {
  let value_text = "";
  this.translate.get(key).subscribe(
  value => {
      value_text = value;
  });
    return value_text;
  }
}
