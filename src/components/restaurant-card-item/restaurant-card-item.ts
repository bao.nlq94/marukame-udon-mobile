import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavController, NavParams} from "ionic-angular";
import {Branch} from "../../models/branch";
import {BranchProvider} from "../../providers/branch/branch";
import { DomSanitizer, SafeResourceUrl, SafeUrl, SafeStyle } from '@angular/platform-browser';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
    selector: 'restaurant-card-item',
    templateUrl: 'restaurant-card-item.html'
})
export class RestaurantCardItemComponent implements OnInit {

    @Input() info: any;
    @Output() onDetailClicked = new EventEmitter();
    @Output() onLocationClicked = new EventEmitter();
    
    reviewSummary: any;
    openTime: string;
    closeTime: string;
    image:SafeStyle;
   
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public branchProvider: BranchProvider,
                private _sanitizer: DomSanitizer,
                private favoriteProvider:FavoriteProvider,
                ) {
        if (this.info) {
            this.info = new Branch();
        }
    }

    ngOnInit() {
        this.reviewSummary = {
            avgRating: 0,
            comments: 0
        };
        this.getSummary();
        this.getOpenTime();
        this.image=this._sanitizer.bypassSecurityTrustStyle(`url(${this.info.bimg_url})`);
    }

    getOpenTime() {
        const bdes = this.info.bdes.split(';');
        this.openTime = bdes[0];
        this.closeTime = bdes[1];
    }

    getSummary() {
        
        this.branchProvider.getReviewSummary(this.info.bid)
            .subscribe((res) => {
                // console.log('getReviewSummary', res);
                this.reviewSummary.avgRating = res['avgRating'];
                this.reviewSummary.comments = res['fiveStarCount'] + res['fourStarCount'] + res['threeStarCount'] + res['twoStarCount'] + res['oneStarCount'];
                // console.log('comment count', this.reviewSummary);
            })
    }

    goDetailRestaurant() {
        this.onDetailClicked.emit();
    }

    goLocation() {
        this.onLocationClicked.emit();
    }

    addFavorite(){
        this.favoriteProvider.updateFavorite(this.info.userId,this.info.bid,2,!this.info.isLike,null).subscribe(res=>{
            this.info.isLike=!this.info.isLike;
            this.favoriteProvider.refeshListFavorite(this.info.userId).then((res)=>{

            });
        })
    }
}
