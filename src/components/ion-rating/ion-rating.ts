import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'ion-rating',
  templateUrl: 'ion-rating.html'
})
export class IonRatingComponent {

  @Input() scorePct: number;
  @Input() fontsize: string;
  @Output() rattingClick=new EventEmitter();
  constructor() {
  }

  setPadding() {
    let styles = {
      'padding-right':'20px'
    };
    return styles;
  }

  clickStar(s: string) {
    this.scorePct = Number(s) * 20;
    this.rattingClick.emit({rate:s});
  }
}
