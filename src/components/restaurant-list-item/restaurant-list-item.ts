import {Component, EventEmitter, Output, OnInit, Input} from '@angular/core';
import {NavController, NavParams} from "ionic-angular";
import {BranchProvider} from "../../providers/branch/branch";
import { DomSanitizer, SafeResourceUrl, SafeUrl, SafeStyle } from '@angular/platform-browser';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AuthProvider } from '../../providers/auth/auth';
@Component({
    selector: 'restaurant-list-item',
    templateUrl: 'restaurant-list-item.html'
})
export class RestaurantListItemComponent implements OnInit{

   
    @Input() info:any;
    @Output() onDetailClicked = new EventEmitter();
    @Output() onLocationClicked = new EventEmitter();

    reviewSummary: any;
    image:SafeStyle;
    constructor(public navCtrl: NavController,
                public branchProvider: BranchProvider,
                public navParams: NavParams,
                private _sanitizer: DomSanitizer,
                private favoriteProvider:FavoriteProvider,
                private aut: AuthProvider) {
        // console.log('Hello RestaurantListItemComponent Component');
        // this.text = 'Hello World';
    }

    ngOnInit(): void {
        this.reviewSummary = {
            avgRating: 0,
            comments: 0
        };
        this.getSummary();
        this.image=this._sanitizer.bypassSecurityTrustStyle(`url(${this.info.bimg_url})`);
    }
    getSummary() {
        
        this.branchProvider.getReviewSummary(this.info.bid)
            .subscribe((res) => {
                // console.log('getReviewSummary', res);
                this.reviewSummary.avgRating = res['avgRating'];
                this.reviewSummary.comments = res['fiveStarCount'] + res['fourStarCount'] + res['threeStarCount'] + res['twoStarCount'] + res['oneStarCount'];
                // console.log('comment count', this.reviewSummary);
            })
    }
    goDetailRestaurant() {
        this.onDetailClicked.emit();
    }

    goLocation() {
        this.onLocationClicked.emit();
    }



}
