import {Component, Input, OnInit} from '@angular/core';
import {GMT_7} from "../../app/app.setting";
import {TranslateService} from "@ngx-translate/core";

declare const require;
const moment = require('moment');

@Component({
    selector: 'open-close-time',
    templateUrl: 'open-close-time.html'
})
export class OpenCloseTimeComponent implements OnInit {

    text: string;
    isOpening: boolean;

    @Input() openTime: string;
    @Input() closeTime: string;

    constructor(public translate: TranslateService) {
        this.text = 'Hello World';
        this.isOpening = true;
    }

    ngOnInit() {
        //console.log('time', this.openTime, this.closeTime);
        this.initLabel();
    }

    initLabel() {
        const today = moment(new Date());

        const nowStr = today.format('YYYY-MM-DD') + 'T' + today.format('HH:mm') + ':00.000' + GMT_7;
        const openTimeStr = today.format('YYYY-MM-DD') + 'T' + this.openTime + ':00.000' + GMT_7;
        const closeTimeStr = today.format('YYYY-MM-DD') + 'T' + this.closeTime + ':00.000' + GMT_7;

        const now = moment(new Date(nowStr));
        const openTime = moment(new Date(openTimeStr));
        const closeTime = moment(new Date(closeTimeStr));

        if (now.isBetween(openTime, closeTime)) {
            this.translate.get('restaurant_list.list_item.opening')
                .subscribe(t => {
                    this.text = t;
                });
        } else {
            this.translate.get('restaurant_list.list_item.closed')
                .subscribe(t => {
                    this.text = t;
                });
        }
    }

}
