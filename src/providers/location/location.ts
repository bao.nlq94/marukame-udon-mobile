import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Platform} from "ionic-angular";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import { Geolocation } from '@ionic-native/geolocation';

export const LOCATION_KEY = 'geolocation';
export class LatLng {
    lat: number;
    lng: number;

    constructor(lat, lng) {
        this.lat = lat;
        this.lng = lng;
    }
}


@Injectable()
export class LocationProvider {

    constructor(public http: HttpClient,
                private geolocation: Geolocation,
                private locationAccuracy: LocationAccuracy,
                private platform: Platform) {
    }

    getCurrentLocation() {
        return new Promise((resolve, reject) => {
            this.getGeoLocation(resolve, reject);
        });
    }

    getGeoLocation(resolve, reject) {
        if (this.platform.is('cordova')) {
            this.locationAccuracy.canRequest().then((canRequest: boolean) => {

                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                    () => {
                        this.geolocation.getCurrentPosition()
                            .then(resp => {
                                console.log('position ', resp);
                                resolve(resp);
                            })
                            .catch(error => {
                                console.error(error);
                                reject(error);
                            })
                    },
                    error => {
                        console.log('Error requesting location permissions', error);
                        reject(error);
                    }
                );

            });
        } else {
            this.geolocation.getCurrentPosition()
                .then(resp => {
                    console.log('position ', resp);
                    resolve(resp);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                })
        }
    }

    getDistanceFromLatLonInKm(org: LatLng, dest: LatLng) {
        const R = 6371; // Radius of the earth in km
        const dLat = this.deg2rad(dest.lat - org.lat);  // deg2rad below
        const dLon = this.deg2rad(dest.lng - org.lng);
        const a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(org.lat)) * Math.cos(this.deg2rad(dest.lat)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    timeDifference(date1, date2) {
        let difference = date1.getTime() - date2.getTime();

        let daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
        difference -= daysDifference * 1000 * 60 * 60 * 24;

        let hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        difference -= hoursDifference * 1000 * 60 * 60;

        let minutesDifference = Math.floor(difference / 1000 / 60);
        difference -= minutesDifference * 1000 * 60;

        let secondsDifference = Math.floor(difference / 1000);

        return {
            days: daysDifference,
            hours: hoursDifference,
            minutes: minutesDifference,
            seconds: secondsDifference
        }
    }

}
