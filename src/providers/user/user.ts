import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestfulProvider } from "../restful";
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";
import 'rxjs/Rx';
import { AppSettings } from "../../app/app.setting";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider extends RestfulProvider {

    constructor(public http: HttpClient) {
        super();
    }

    getUserDetail(userId: number): Observable<any> {
        let url = this.getUrlNoToken(`/user/profile/${userId}`);
        return this.http.get(url, { headers: AppSettings.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getCardScore(userId: number): Observable<any> {
        let url = this.getUrlNoToken(`/user/cardscore/${userId}`);
        return this.http.get(url, { headers: AppSettings.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    update(userId: number, data: any): Observable<any> {
        let url = this.getUrlNoToken(`/user/profile/update/${userId}`);
        // console.log(url);
        // console.log(data);
        return this.http.post(url, data, { headers: AppSettings.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }
}
