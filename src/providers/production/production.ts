import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestfulProvider } from '../restful';
import { AppSettings } from '../../app/app.setting';

/*
  Generated class for the ProductionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductionProvider extends RestfulProvider{

  constructor(public http: HttpClient) {
    super();
  }

  submitReview(param: any) {
    let url = this.getUrlNoToken('/business/review/submit');
    console.log(param);
    return this.http.post(url, param, {headers: AppSettings.getAuthenticationHeaders()})
        .map(this.extractData)
        .catch(this.handleError);
  }

  getReviewList(branchId: number, page: number,size:number,entityType:number) {
    let url = this.getUrlNoToken('/business/review/list?branchId=' + branchId + '&page=' + page+'&itemPerPage='+size+'&entityType='+entityType);
    return this.http.post(url, {}, {headers: AppSettings.getAuthenticationHeaders()})
        .map(this.extractData)
        .catch(this.handleError);
  }
}
