import {HttpErrorResponse} from '@angular/common/http';
import {API_ENDPOINT, AppSettings} from "../app/app.setting";

export class RestfulProvider {

    protected getUrlNoToken(link, pram: any = null) {
        let url = "";
        if (pram != null) {
            url = `${API_ENDPOINT}${link}${pram}`;
        }
        else {
            url = `${API_ENDPOINT}${link}`;
        }
        return url;
    }

    protected handleError(error: HttpErrorResponse): Promise<any> {
        // console.log('An error occurred', error.message); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    protected extractData(res: any) {
        // console.log('extract data ', res);
        let body = res;
        // console.log('body type ', typeof body);
        if (typeof body !== "object")
            body = JSON.parse(body);
        return body || {};
    }

}
