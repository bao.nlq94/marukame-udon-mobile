import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AlertController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";


@Injectable()
export class AlertProvider {

    btnOk: string;
    btnCancel: string;

    basicAlert: any;
    prompt: any;

    constructor(public http: HttpClient,
                public alertCtrl: AlertController,
                public translate: TranslateService) {
        // // console.log('Hello AlertProvider Provider');

        this.btnOk = 'common.btn_ok';
        this.btnCancel = 'common.btn_cancel';
    }

    showBasicAlert(title, subTitle, btnOk?, okCallback?) {
        if (!btnOk) {
            btnOk = this.btnOk;
        }

        const alertContent = {
            title: '',
            subTitle: '',
            btnOk: ''
        };

        this.translate.get(title)
            .subscribe(t => {
                alertContent.title = t;

                this.translate.get(subTitle)
                    .subscribe(s => {
                        alertContent.subTitle = s;

                        this.translate.get(btnOk)
                            .subscribe(bOk => {
                                alertContent.btnOk = bOk;

                                this.basicAlert = this.alertCtrl.create({
                                    title: alertContent.title,
                                    subTitle: alertContent.subTitle,
                                    buttons: [{
                                        text: alertContent.btnOk,
                                        handler: data => {
                                            // console.log('Ok clicked');
                                            if (okCallback) {
                                                okCallback();
                                            }
                                        }
                                    }]
                                });
                                this.basicAlert.present();
                            })
                    })
            })
    }

    showPrompt(title, message, inputTitle, inputPlaceholder?, btnOk?, okCallback?) {
        if (!btnOk) {
            btnOk = this.btnOk;
        }

        this.translate.get(title)
            .subscribe(txtTitle => {

                this.translate.get(message)
                    .subscribe(txtMsg => {

                        this.translate.get(btnOk)
                            .subscribe(txtOK => {

                                this.translate.get(inputTitle)
                                    .subscribe(ipTitle => {

                                        this.translate.get(inputPlaceholder)
                                            .subscribe(ipPlaceholder => {
                                                this.prompt = this.alertCtrl.create({
                                                    title: txtTitle,
                                                    message: txtMsg,
                                                    inputs: [
                                                        {
                                                            name: ipTitle,
                                                            placeholder: ipPlaceholder
                                                        },
                                                    ],
                                                    buttons: [
                                                        {
                                                            text: 'Cancel',
                                                            handler: data => {
                                                                // console.log('Cancel clicked');
                                                            }
                                                        },
                                                        {
                                                            text: txtOK,
                                                            handler: data => {
                                                                console.log('Saved clicked', data);
                                                                if (okCallback) okCallback(data[ipTitle]);
                                                            }
                                                        }
                                                    ]
                                                });
                                                this.prompt.present();
                                            });
                                    });

                            })
                    })

            })
    }

}
