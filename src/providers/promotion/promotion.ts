import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {RestfulProvider} from "../restful";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import {AppSettings} from "../../app/app.setting";

/*
  Generated class for the PromotionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PromotionProvider extends RestfulProvider{

  constructor(public http: HttpClient) {
    super();
  }

  getPromotionNotification(): Observable<any> {
    let url=this.getUrlNoToken("/promotion/list/featured");
    return this.http.get(url, {headers: AppSettings.getHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
  }

  

}
