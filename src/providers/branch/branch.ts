import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {RestfulProvider} from "../restful";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import {AppSettings} from "../../app/app.setting";


@Injectable()
export class BranchProvider extends RestfulProvider {

    constructor(public http: HttpClient) {
        super();
    }

    //GET /api/v{version}/business
    getBranchBusiness(): Observable<any> {
        let url = this.getUrlNoToken('/business/list');
        return this.http.get(url, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    //GET /api/v{version}/branch/detail/{branch}/menu
    getBranchMenu(branch: number): Observable<any> {
        let url = this.getUrlNoToken('/business/branch/detail/' + branch + '/menu');
        // console.log(url);
        return this.http.get(url, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    // Get /api/v1.0/business/branch/search
    searchBranch(page:number,size:number,textSearch:string,lat:number,long:number){
        let url=this.getUrlNoToken(`/business/branch/search?page=${page}&size=${size}&condition=DIF;SBD&keyword=${textSearch};${lat},${long}`);
        console.log(url)
        return this.http.get(encodeURI(url), {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    //GET /api/v{version}/branch/detail/{branch}/{menu}
    getBranchCategories(branch): Observable<any> {
        let url = this.getUrlNoToken('/business/branch/detail/' + branch + '/menu');
        return this.http.get(url, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    getDishesByCategory(branch:number,menu:number,page:number,size:number){
        let url = this.getUrlNoToken(`/business/branch/detail/${branch}/${menu}?page=${page}&size=${size}`);
        return this.http.get(url, {headers: AppSettings.getAuthenticationHeaders()})
        .map(this.extractData)
        .catch(this.handleError);
    }

    submitFeedback(param: any) {
        let url = this.getUrlNoToken('/business/submitFeedback');
        return this.http.post(url, param, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    getReviewSummary(branchId: number) {
        let url = this.getUrlNoToken('/business/review/summary?branchId=' + branchId);
        return this.http.get(url, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    getReviewList(branchId: number, page: number,size:number) {
        let url = this.getUrlNoToken('/business/review/list?branchId=' + branchId + '&page=' + page+'&itemPerPage='+size);
        return this.http.post(url, {}, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }

    submitReview(param: any) {
        let url = this.getUrlNoToken('/business/review/submit');
        return this.http.post(url, param, {headers: AppSettings.getAuthenticationHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
    }
}
