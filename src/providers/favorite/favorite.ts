import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {RestfulProvider} from "../restful";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import {AppSettings} from "../../app/app.setting";0
/*
  Generated class for the FavoriteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export const LIST_FAVORITE = 'list_favorite';
@Injectable()
export class FavoriteProvider extends RestfulProvider{

  constructor(public http: HttpClient) {
    super();
  }


  updateFavorite(userId:number,entityId:number,entityType:number,isLike:boolean,categoryId:number): Observable<any> {
    let url=this.getUrlNoToken(`/favorite/updateLike`);
    let  data={
      ObjectID:userId,
      EntityID:entityId,
      EntityType:entityType,
      isLike:isLike,
      CategoryId:categoryId,
    }
    return this.http.post(url,data, {headers: AppSettings.getHeaders()})
            .map(this.extractData)
            .catch(this.handleError);
  }

  getListFavorite(userId:number){
    let url=this.getUrlNoToken(`/favorite/getlistfavorite/${userId}`);
    return this.http.get(url, {headers: AppSettings.getHeaders()})
               .map(this.extractData)
               .catch(this.handleError)
  }

  listFavorite(userId:number):Promise<any>{
    return new Promise((resolve)=>{
      if(localStorage.getItem(LIST_FAVORITE)!==null){
        //resolve(true);
        let listFavorite=localStorage.getItem(LIST_FAVORITE);
        resolve(JSON.parse(listFavorite));
      }else{
        this.getListFavorite(userId).subscribe((res)=>{
          if(res.resultCode==="OK"){
            let  dataFavorite={
              FavoriteFoods:res.FavoriteFoods,
              FavoriteRestaurants:res.FavoriteRestaurants,
            };
            localStorage.setItem(LIST_FAVORITE,JSON.stringify(dataFavorite));
            resolve(dataFavorite);
          }else{
          }
        })
      }
    })
  }

  refeshListFavorite(userId:number):Promise<any>{
    localStorage.removeItem(LIST_FAVORITE)
    return this.listFavorite(userId)
  }
}
