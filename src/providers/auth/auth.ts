import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { RestfulProvider } from '../restful';
import { AppSettings } from '../../app/app.setting';

export const IS_AUTHENTICATED_KEY = 'authenticated';
export const USER_KEY = 'current_user';
export const SOCIAL_KEY = 'social';
export const USER_TOKEN = 'token';

@Injectable()
export class AuthProvider extends RestfulProvider {

    constructor(public http: HttpClient) {
        super();
    }

    isLoggedIn(): boolean {
        const isLogged = localStorage.getItem(IS_AUTHENTICATED_KEY);
        return isLogged === 'true';
    }

    getGoogleUserInfo(accessToken: string) {
        const header = new HttpHeaders({
            'Authorization': `Bearer ${accessToken}`
        })

        const url = `https://www.googleapis.com/oauth2/v2/userinfo?fields=email%2Cgender%2Cid%2Cname%2Cpicture&key=${accessToken}`
        return this.http.get(url, {headers: header})
            .map(this.extractData)
            .catch(this.handleError);
    }

    loginWithGoogle(data) {
        let url = this.getUrlNoToken('/auth/signin/social?social=gg');
        return this.http.post(url, JSON.stringify(data), { headers: AppSettings.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    loginWithFacebook(data) {
        let url = this.getUrlNoToken('/auth/signin/social?social=fb');
        // console.log(url);
        return this.http.post(url, JSON.stringify(data), { headers: AppSettings.getHeaders() })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getUser(): any {
        let user = localStorage.getItem(USER_KEY);
        console.log(user);
        if (user) {
            return JSON.parse(user.toString());
        }
    }

    logout() {
        return Observable.create(observer => {
            localStorage.removeItem(IS_AUTHENTICATED_KEY);
            localStorage.removeItem(USER_KEY);
            observer.next({});
        });
    }
}
