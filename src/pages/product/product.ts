import {Component, OnInit, Input} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { ProductionProvider} from '../../providers/production/production';


@IonicPage({
    name: 'page-product',
    segment: 'page-product'
})
@Component({
    selector: 'page-product',
    templateUrl: 'product.html',
})
export class ProductPage implements OnInit {

    dataProduction:any;
    rattingComment:number;
    textComment: string;
    reviewList: any;
    isViewAllReview: boolean;
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private authenProvider: AuthProvider,
                private favoriteProvider: FavoriteProvider,
                private productionProvider:ProductionProvider
                ) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad ProductPage');
    }

    ratingClick(data) {
        this.rattingComment = Number(data.rate);
    }

    ngOnInit(): void {
        //throw new Error("Method not implemented.");
        this.reviewList=[];
        this.dataProduction=this.navParams.get("data");
        console.log(this.dataProduction);
        this.getReviewList();

    }

    submitComment(){
        let user = this.authenProvider.getUser();
        console.log(user.userId);
        this.productionProvider.submitReview({
            branchId: this.dataProduction.id,
            reviewContent: this.textComment, 
            ratingScore: this.rattingComment,
            userId: user.userId,
            entityType:2,
        }).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.rattingComment = 0;
                this.textComment = "";
                this.getReviewList();
                // this.getSummary();
            }
        })
    }

    getReviewList() {
        this.reviewList = [];
        console.log(this.dataProduction.id);
        this.productionProvider.getReviewList(this.dataProduction.id, 1, 5,2).subscribe((res) => {
            if (res.resultCode === "OK") {
                //console.log(res.result);
                this.reviewList = res.result;
            }
        })
    }

    back() {
        try {
            this.navCtrl.pop();
        } catch (e) {
            this.navCtrl.setRoot('page-tabs', {tab: 1});
        }
    }

}
