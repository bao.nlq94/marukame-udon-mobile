import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductPage } from './product';
import {ComponentsModule} from "../../components/components.module";
import {TranslateModule, TranslateService} from "@ngx-translate/core";
@NgModule({
  declarations: [
    ProductPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ProductPage),
    TranslateModule.forChild()
  ],
  providers: [
    TranslateService,
  ]
})
export class ProductPageModule {}
