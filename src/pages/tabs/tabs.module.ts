import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabsPage} from './tabs';
import {AuthProvider} from "../../providers/auth/auth";

@NgModule({
    declarations: [
        TabsPage,
    ],
    imports: [
        IonicPageModule.forChild(TabsPage),
    ],
    providers: [
        AuthProvider
    ]
})
export class TabsPageModule {
}
