import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Tabs} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";


@IonicPage({
    name: 'page-tabs',
    segment: 'page-tabs',
})
@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html',
})
export class TabsPage {
    tab1Root = 'page-favorite';
    tab2Root = 'page-home';
    tab3Root = 'page-my-profile';
    tab4Root = 'page-notification';

    @ViewChild('myTabs') tabRef: Tabs;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public auth: AuthProvider) {

    }

    ionViewWillEnter() {
        //console.log('ionViewWillEnter', this.auth.isLoggedIn());
        if (!this.auth.isLoggedIn())
            this.navCtrl.push('page-login');
    }

    ionViewDidEnter() {
        this.activeTab();
    }

    activeTab() {
        let activeTab = this.navParams.get('tab');
        if (!activeTab)
            activeTab = 1;
        this.tabRef.select(activeTab);
    }

    // ionViewCanEnter(): boolean {
    //     console.log('ionViewCanEnter');
    //     return this.auth.isLoggedIn();
    // }

}
