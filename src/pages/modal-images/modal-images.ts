import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Slides, ViewController} from 'ionic-angular';

/**
 * Generated class for the ModalImagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'page-modal-images',
  segment:'page-modal-images'
})
@Component({
  selector: 'page-modal-images',
  templateUrl: 'modal-images.html',
})
export class ModalImagesPage {
  @ViewChild(Slides) slides: Slides;
  currentIndex:string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtl: ViewController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ModalImagesPage');
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    //console.log('Current index is', currentIndex);
    this.currentIndex = currentIndex+'';
  }

  goToSlide(s) {
    this.slides.slideTo(Number(s), 500);
  }

  goDismiss() {
    this.viewCtl.dismiss();
  }
}
