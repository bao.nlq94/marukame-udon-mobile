import { Component, Input, OnInit } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, Item } from 'ionic-angular';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { BranchProvider } from '../../providers/branch/branch';
import { AuthProvider } from '../../providers/auth/auth';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AppSettings, API_ABS } from '../../app/app.setting';

@IonicPage({
    name: 'page-detail-restaurant',
    segment: 'page-detail-restaurant'
})
@Component({
    selector: 'page-detail-restaurant',
    templateUrl: 'detail-restaurant.html',
})
export class DetailRestaurantPage implements OnInit {

    info: any;
    image: SafeStyle;
    reviewSummary: any;
    rattingComment: number;
    textComment: string;
    reviewList: any;
    isViewAllReview: boolean;
    urlDetail: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private _sanitizer: DomSanitizer,
        private branchProvider: BranchProvider,
        private authenProvider: AuthProvider,
        private favoriteProvider: FavoriteProvider,
        private sanitizer: DomSanitizer ) {

    }
    ngOnInit(): void {
        this.info = this.navParams.get("info");
        console.log(this.info);
        this.urlDetail = this.sanitizer.bypassSecurityTrustResourceUrl(this.info.babout_url);
        this.reviewList = [];
        this.image = this._sanitizer.bypassSecurityTrustStyle(`url(${this.info.bimg_url})`);
        this.reviewSummary = {
            avgRating: 0,
            comments: 0,
            perStart: 0,
        },
            this.isViewAllReview = false;
        this.getSummary();
        this.getReviewList();
    }
    ionViewDidLoad() {
        //console.log('ionViewDidLoad DetailRestaurantPage');
    }

    goShowModal() {
        let modal = this.modalCtrl.create('page-modal-images');
        modal.present();
    }

    goViewAll() {
        this.navCtrl.push("page-viewall-review", { bid: this.info.bid, totalComment: this.reviewSummary.comments });
    }

    getSummary() {
        this.branchProvider.getReviewSummary(this.info.bid)
            .subscribe((res) => {
                //console.log(res);
                this.reviewSummary.avgRating = res['avgRating'];
                this.reviewSummary.perStart = this.reviewSummary.avgRating * 100;
                this.reviewSummary.comments = res['fiveStarCount'] + res['fourStarCount'] + res['threeStarCount'] + res['twoStarCount'] + res['oneStarCount'];
                this.isViewAllReview = res.totalComments > 5;
                //console.log( this.isViewAllReview)
            })
    }

    ratingClick(data) {
        this.rattingComment = Number(data.rate);
    }

    getReviewList() {
        this.reviewList = [];
        this.branchProvider.getReviewList(this.info.bid, 1, 5).subscribe((res) => {
            if (res.resultCode === "OK") {
                //console.log(res.result);
                this.reviewList = res.result;
            }
        })
    }

    goLocation() {
        this.navCtrl.push('page-location', { dataBranch: this.info });
    }

    submitComment() {
        let user = this.authenProvider.getUser();
        //console.log(user);
        this.branchProvider.submitReview({
            branchId: this.info.bid,
            reviewContent: this.textComment, ratingScore: this.rattingComment,
            userId: user.id
        }).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.rattingComment = 0;
                this.textComment = "";
                this.getReviewList();
                this.getSummary();
            }
        })
    }

    addFavorite() {

        this.favoriteProvider.updateFavorite(this.info.userId, this.info.bid, 2, !this.info.isLike, null).subscribe(res => {
            this.info.isLike = !this.info.isLike;
            this.favoriteProvider.refeshListFavorite(this.info.userId).then((res) => {

            })
        })
    }
}
