import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailRestaurantPage } from './detail-restaurant';
import {ComponentsModule} from "../../components/components.module";
import {TranslateModule, TranslateService} from "@ngx-translate/core";
@NgModule({
  declarations: [
    DetailRestaurantPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(DetailRestaurantPage),
    TranslateModule.forChild()
  ],
  providers: [
    TranslateService,
  ]
})
export class DetailRestaurantPageModule {}
