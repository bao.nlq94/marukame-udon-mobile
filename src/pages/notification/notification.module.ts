import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationPage } from './notification';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService, TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationPage),
    TranslateModule.forChild()
  ],
  providers: [
    PromotionProvider,
    CookieService,
    TranslateService,
  ]
})
export class NotificationPageModule {}
