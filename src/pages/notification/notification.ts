import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PromotionProvider } from '../../providers/promotion/promotion';

@IonicPage({
    name: 'page-notification',
    segment: 'page-notification',
})
@Component({
    selector: 'page-notification',
    templateUrl: 'notification.html',
})
export class NotificationPage implements OnInit {

    listNotification: any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private promotionProvider: PromotionProvider) {
    }

    ngOnInit(): void {
        this.listNotification = [];
        this.getNotification();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad NotificationPage');
    }

    getNotification() {
        this.promotionProvider.getPromotionNotification().subscribe((res) => {
            if (res.resultCode === "OK") {
                this.listNotification = res.list;
            }
        })
    }

    goDetail(item) {
        this.navCtrl.push('page-notification-detail', {'item': item});
    }

}
