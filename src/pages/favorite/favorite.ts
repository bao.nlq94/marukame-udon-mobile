import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AuthProvider } from '../../providers/auth/auth';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@IonicPage({
    name: 'page-favorite',
    segment: 'page-favorite',
})
@Component({
    selector: 'page-favorite',
    templateUrl: 'favorite.html',
})
export class FavoritePage {

    activeTab: string;
    favoriteFoods: any;
    favoriteRestaurants: any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private favoriteProvider: FavoriteProvider,
        private _sanitizer: DomSanitizer,
        private aut: AuthProvider) {
    }

    ngOnInit() {
        this.activeTab = 'dish';
        this.favoriteFoods = [];
        this.favoriteRestaurants = [];
        // this.getFavoriteList();
    }
    ionViewWillEnter() {
    }
    ionViewDidLoad() {
        this.getFavoriteList();
        //console.log('ionViewDidLoad FavoritePage');
    }

    switchTab(name) {
        this.activeTab = name;
    }
    goDetailRestaurant(data) {
        this.navCtrl.push('page-detail-restaurant', { info: data });
    }

    goLocation() {
        this.navCtrl.push('page-location');
    }

    getFavoriteList() {
        const user = this.aut.getUser();
        console.log('local user', user);
        if (user) {
            let userId = user.id;
            this.favoriteFoods = [];
            this.favoriteProvider.listFavorite(userId).then((res) => {
                if (res.FavoriteFoods) {
                    //this.favoriteFoods=res.FavoriteFoods;
                    res.FavoriteFoods.forEach(item => {
                        let itemFoot = <any>{};
                        let itemimg_url: SafeStyle;
                        itemimg_url = this._sanitizer.bypassSecurityTrustStyle(`url("${(item.itemimg_url)}")`);
                        itemFoot.itemid = item.itemid;
                        itemFoot.itemname = item.itemname;
                        itemFoot.itemprice = item.itemprice;
                        itemFoot.itemimg_url = itemimg_url;
                        itemFoot.currency = item.currency;
                        this.favoriteFoods.push(itemFoot);
                    });
                }

                if (res.FavoriteRestaurants) {
                    this.favoriteRestaurants = res.FavoriteRestaurants;
                    this.favoriteRestaurants.forEach(item => {
                        item.userId = userId;
                        item.isLike = true;
                    });
                }
            })
        }
    }

    goProduction(dataProduction:any) {
        let dataFood={
            id:dataProduction.itemid,
            name:dataProduction.itemname,
            price:dataProduction.itemprice,
            currency:dataProduction.currency,
            img:dataProduction.itemimg_url,
        }
        this.navCtrl.push('page-product',{data:dataFood});
    }

}
