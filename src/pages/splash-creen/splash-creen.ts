import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';


@IonicPage({
    name: 'page-splash-creen',
    segment: 'page-splash-creen',
})
@Component({
    selector: 'page-splash-creen',
    templateUrl: 'splash-creen.html',
})
export class SplashCreenPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SplashCreenPage');
    }

    goLanguage(s) {
        this.navCtrl.push('page-login');
    }
}
