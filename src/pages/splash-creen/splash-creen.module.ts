import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SplashCreenPage } from './splash-creen';

@NgModule({
  declarations: [
    SplashCreenPage,
  ],
  imports: [
    IonicPageModule.forChild(SplashCreenPage),
  ],
})
export class SplashCreenPageModule {}
