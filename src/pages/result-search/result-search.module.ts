import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultSearchPage } from './result-search';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    ResultSearchPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ResultSearchPage),
  ],
})
export class ResultSearchPageModule {}
