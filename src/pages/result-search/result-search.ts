import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';


@IonicPage({
    name: 'page-result-search',
    segment: 'page-result-search',
})
@Component({
    selector: 'page-result-search',
    templateUrl: 'result-search.html',
})
export class ResultSearchPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ResultSearchPage');
    }

    goProduct() {
        this.navCtrl.push('page-product');
    }

    back() {
        try {
            this.navCtrl.pop();
        } catch (e) {
            this.navCtrl.setRoot('page-tabs', {tab: 1});
        }
    }
}
