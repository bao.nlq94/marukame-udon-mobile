import {Component, ElementRef, ViewChild, OnInit} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {LocationProvider} from "../../providers/location/location"
declare const google;

@IonicPage({
    name: 'page-location',
    segment: 'page-location',
})
@Component({
    selector: 'page-location',
    templateUrl: 'location.html',
})
export class LocationPage implements OnInit{
    
    @ViewChild('map') mapElement: ElementRef;
    map: any;
    dataBranch: any = {};
    lat: number = 43.0741904;
    lng: number = -89.3809802;
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    marker: any;
    start = 'chicago, il';
    end = 'chicago, il';
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public appCtrl: App,
                private localtion:LocationProvider) {

    }

    ngOnInit(): void {
        this.dataBranch = this.navParams.get('dataBranch');
        console.log('branch ', this.dataBranch);
        this.lat = this.dataBranch.blat;
        this.lng = this.dataBranch.blng;
        this.initMap();
    }

    ionViewDidLoad() {
        
    }

    initMap() {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: {lat: this.lat, lng: this.lng}
        });
        this.directionsDisplay.setMap(this.map);
        this.addMarkersToMap({
            latitude: this.lat,
            longitude: this.lng,
            name: this.dataBranch.bname
        });
        this.calculateAndDisplayRoute();
    }

    addMarkersToMap(museum) {
        const position = new google.maps.LatLng(museum.latitude, museum.longitude);
        this.marker = new google.maps.Marker({position: position, title: museum.name});
        this.marker.setMap(this.map);
        
    }

    calculateAndDisplayRoute() {
        //console.log('calculate route ');
        this.localtion.getCurrentLocation()
            .then((resp) => {
                //console.log('get current geolocation ', resp);
                this.drawDirection(resp);
            })
            .catch(error => console.error(error));
    }

    drawDirection(resp) {
        console.log('position ', resp);
        this.directionsService.route({
            origin: new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude),
            destination: new google.maps.LatLng(this.lat, this.lng),
            travelMode: 'DRIVING'
        }, (response, status) => {
            if (status === 'OK') {
                //console.log('current position response ' + JSON.stringify(response));
                this.removeMarker();
                this.directionsDisplay.setDirections(response);
            } else {
                //console.log('Directions request failed due to ' + status);
            }
        });
    }
    removeMarker() {
        this.marker.setMap(null);
    }
    // loadMap() {

    //     let latLng = new google.maps.LatLng(10.776020, 106.677973);
    //     let latLng1 = new google.maps.LatLng(10.779136, 106.678213);
    //     let latLng2 = new google.maps.LatLng(10.775348, 106.680458);
    //     let latLng3 = new google.maps.LatLng(10.776128, 106.675597);
    //     let latLng4 = new google.maps.LatLng(10.773515, 106.675031);

    //     let mapOptions = {
    //         center: latLng,
    //         zoom: 15,
    //         disableDefaultUI: true,
    //         mapTypeId: google.maps.MapTypeId.ROADMAP
    //     };

    //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    //     const marker = new google.maps.Marker({
    //         position: latLng,
    //         map: this.map,
    //     });

    //     const marker1 = new google.maps.Marker({
    //         position: latLng1,
    //         map: this.map,
    //         icon: 'assets/icon/svg/pin.png'
    //     });

    //     const marker2 = new google.maps.Marker({
    //         position: latLng2,
    //         map: this.map,
    //         icon: 'assets/icon/svg/pin.png'
    //     });

    //     const marker3 = new google.maps.Marker({
    //         position: latLng3,
    //         map: this.map,
    //         icon: 'assets/icon/svg/pin.png'
    //     });

    //     const marker4 = new google.maps.Marker({
    //         position: latLng4,
    //         map: this.map,
    //         icon: 'assets/icon/svg/pin.png'
    //     });

    // }

    back() {
        if (this.navCtrl.canGoBack())
            this.navCtrl.pop();
        else
            this.appCtrl.getRootNav().push('page-tabs');
    }
}
