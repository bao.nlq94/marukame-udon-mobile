import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyQrcodePage } from './my-qrcode';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  declarations: [
    MyQrcodePage,
  ],
  imports: [
    IonicPageModule.forChild(MyQrcodePage),
    QRCodeModule,
  ],
})
export class MyQrcodePageModule {}
