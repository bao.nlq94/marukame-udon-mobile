import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage({
    name: 'page-my-qrcode',
    segment: 'page-my-qrcode'
})
@Component({
    selector: 'page-my-qrcode',
    templateUrl: 'my-qrcode.html',
})
export class MyQrcodePage implements OnInit {

    cardNo: string;
    userDetail: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams) {
    }

    ngOnInit(): void {
        this.cardNo = this.navParams.get("cardNo");
        this.userDetail = this.navParams.get("dataUser");
        console.log('user', this.userDetail);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MyQrcodePage');
    }

}
