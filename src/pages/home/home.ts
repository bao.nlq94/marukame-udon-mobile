import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { App, IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { BranchProvider } from '../../providers/branch/branch';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AuthProvider } from '../../providers/auth/auth';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import {GOOGLE_ANALYTICS_TRACKID} from '../../app/app.setting';

@IonicPage({
    name: 'page-home',
    segment: 'page-home'
})
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage implements OnInit {

    @ViewChild('groupSearch') searchRef: ElementRef;
    categories: any;
    activatedCategory: number;
    lishOfDishes: any;
    brandId: number;
    totalPage: number;
    page: number;
    size = 10;
    listFavoriteFoods: any;
    userId: number;
    constructor(public navCtrl: NavController,
                public appCtrl: App,
                public navParams: NavParams,
                private serviceBrand:BranchProvider,
                private _sanitizer: DomSanitizer,
                private favoriteProvider:FavoriteProvider,
                private aut: AuthProvider,
                private ga:GoogleAnalytics) {
                    
    }

    ngOnInit() {
        this.categories = [];
        this.lishOfDishes = [];
        this.listFavoriteFoods = [];
        this.initCategoryList();
    }

    ionViewDidEnter(){
      
    }

    initCategoryList() {
        this.categories = [];
        this.activatedCategory = 0;
    }


    changeCategory(catId: number) {
        this.activatedCategory = catId;
        this.lishOfDishes = [];
        this.page = 1;
        this.totalPage = 0;
        this.getDishesByCategory();
    }

    ionViewDidLoad() {
        console.log('can go back', this.navCtrl.canGoBack());

        this.brandId = 0;
        this.totalPage = 0;
        this.page = 1;
        const user = this.aut.getUser();
        console.log("user", user);
        if (user) {
            this.userId = user.id;
            this.favoriteProvider.listFavorite(this.userId).then((res) => {
                console.log('favoreite foods', res)
                this.listFavoriteFoods = res.FavoriteFoods;
                this.getBranchId().then((res) => {
                    this.getCategories();
                });
            })
        } else {
            this.appCtrl.getRootNav().setRoot('page-login');
        }

        this.ga.startTrackerWithId(GOOGLE_ANALYTICS_TRACKID).then(() => {
            this.ga.trackView("Home");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
          }).catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    getGroupSearchWidth() {
        if (!this.navCtrl.canGoBack()) {
            return '100%';
        } else {
            return 'calc(100% - 58px);';
        }
    }

    getBranchId(): Promise<boolean> {
        return new Promise<boolean>((resovle) => {
            this.serviceBrand.getBranchBusiness().subscribe((res) => {
                if (res.resultCode === "OK") {
                    let fristBuiness = res.result[0];
                    this.brandId = fristBuiness.contain[0].bid;
                    resovle(true);
                }
            })
        })
    }

    getCategories() {
        this.serviceBrand.getBranchMenu(this.brandId).subscribe((res) => {
            if (res.resultCode === "OK") {
                res.result.forEach(item => {
                    let itemCategory = <any>{};
                    itemCategory.id = item.menuid;
                    itemCategory.name = item.menuname;
                    this.categories.push(itemCategory);
                });
                this.activatedCategory = this.categories[0].id;
                this.getDishesByCategory();
            }
        })
    }

    getDishesByCategory(): Promise<boolean> {
        return new Promise<boolean>((resovle) => {
            this.serviceBrand.getDishesByCategory(this.brandId, this.activatedCategory, this.page, this.size)
                .subscribe((res) => {
                    if (res.resultCode === "OK") {
                        res.result.CONTAIN.forEach(item => {
                            let itemDishes = <any>{};
                            let image: SafeStyle;
                            let isLike = false;
                            image = this._sanitizer.bypassSecurityTrustStyle(`url("${(item.itemimg_url)}")`);
                            if (this.listFavoriteFoods) {
                                let itemFavorite = this.listFavoriteFoods.find(x => x.itemid === item.itemid);
                                if (itemFavorite) {
                                    isLike = true;
                                } else {
                                    isLike = false;
                                }
                            }
                            itemDishes.id = item.itemid;
                            itemDishes.name = item.itemname;
                            itemDishes.price = item.itemprice;
                            itemDishes.img = image;
                            itemDishes.currency = item.currency;
                            itemDishes.isLike = isLike;
                            this.lishOfDishes.push(itemDishes);
                        });
                        this.totalPage = res.result.PAGING[0].totalPage;
                        resovle(true);
                    }
                })
        })
    }

    doInfinite(infiniteScroll) {
        if (this.page < this.totalPage) {
            this.page += 1;
            this.getDishesByCategory().then(() => {
                infiniteScroll.complete();
            })
        } else {
            infiniteScroll.complete();
        }
    }

    goProduction(bid: number,dataProduction:any) {
        //console.log(dataProduction);
        this.navCtrl.push('page-product',{data:dataProduction});
    }

    goRestaurantList() {
        this.navCtrl.push('page-list-restaurant');
    }

    back() {
        this.appCtrl.getRootNav().push('page-tabs');
    }

    addFavorite(itemDishes) {
        this.favoriteProvider.updateFavorite(this.userId, itemDishes.id, 1, !itemDishes.isLike, this.activatedCategory).subscribe(res => {
            //console.log(res);
            if (this.lishOfDishes) {
                let itemDishesChange = this.lishOfDishes.find(x => x.id === itemDishes.id);
                if (itemDishesChange) {
                    itemDishesChange.isLike = !itemDishes.isLike;
                }
                this.favoriteProvider.refeshListFavorite(this.userId).then((res) => {

                });
            }
        })
    }
}
