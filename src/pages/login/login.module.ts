import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {LoginPage} from './login';
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {AuthProvider} from "../../providers/auth/auth";
import {AlertProvider} from "../../providers/alert/alert";

@NgModule({
    declarations: [
        LoginPage,
    ],
    imports: [
        IonicPageModule.forChild(LoginPage),
        TranslateModule.forChild()
    ],
    providers: [
        Facebook,
        GooglePlus,
        TranslateService,
        AuthProvider,
        AlertProvider
    ]
})
export class LoginPageModule {
}
