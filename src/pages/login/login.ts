import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { GooglePlus } from "@ionic-native/google-plus";
import { User } from "../../models/user";
import { AuthProvider, USER_KEY, IS_AUTHENTICATED_KEY, SOCIAL_KEY, USER_TOKEN } from "../../providers/auth/auth";
import { AlertProvider } from "../../providers/alert/alert";
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { GOOGLE_ANALYTICS_TRACKID } from '../../app/app.setting';

@IonicPage({
    name: 'page-login',
    segment: 'page-login'
})
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private fb: Facebook,
        private googlePlus: GooglePlus,
        private auth: AuthProvider,
        private alert: AlertProvider,
        private ga: GoogleAnalytics) {
    }

    ionViewWillEnter() {
        console.log('ionViewDidLoad LoginPage');
        if (this.auth.isLoggedIn()) {
            this.goTabPage();
        }

        this.ga.startTrackerWithId(GOOGLE_ANALYTICS_TRACKID).then(() => {
            this.ga.trackView("login");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
          }).catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    loginWithFacebook() {
        const t = this;
        this.fb.login(['public_profile', 'user_gender', 'email'])
            .then((res: FacebookLoginResponse) => {
                console.log('Logged into Facebook!', res);
                if (res.status === 'connected') {

                    this.fb.api('me?fields=id,name,email,address,picture,gender', ['public_profile', 'user_gender', 'email', 'user_birthday'])
                        .then(pres => {
                            console.log('me ', pres);

                            const user = new User();
                            user.accessToken = res.authResponse.accessToken;
                            user.linkedAccountId = res.authResponse.userID;
                            user.fullname = pres.name;
                            user.email = pres.email;

                            const reqData = {
                                id: user.linkedAccountId,
                                email: pres.email,
                                name: pres.name,
                                picture: pres.picture,
                                gender: pres.gender,
                                birthday: ''
                            }

                            t.auth.loginWithFacebook({ data: JSON.stringify(reqData) })
                                .subscribe(r => {
                                    const data = r.datafb;
                                    localStorage.setItem(USER_KEY, JSON.stringify(r['profile'][0]));
                                    localStorage.setItem(SOCIAL_KEY, JSON.stringify(r['datafb']));
                                    localStorage.setItem(USER_TOKEN, r['token']);
                                    localStorage.setItem(IS_AUTHENTICATED_KEY, 'true');
                                    t.goTabPage();
                                })
                        });
                }
            })
            .catch(e => {
                console.log('Error logging into Facebook', e);
                this.alert.showBasicAlert('login.error_alert.title', 'login.error_alert.sub_title');
            });
    }

    loginWithGoogle() {
        console.log('loginWithGoogle')
        this.googlePlus.login({})
            .then(res => {
                const user = new User();
                user.fullname = res.displayName;
                user.email = res.email;
                user.accessToken = res.accessToken;
                user.linkedType = 'google';

                this.auth.getGoogleUserInfo(user.accessToken)
                    .subscribe(r1 => {
                        console.log('gg user info ', r1);
                        const reqData = {
                            id: r1.id,
                            email: r1.email,
                            name: r1.name,
                            picture: r1.picture,
                            gender: r1.gender,
                            birthday: ''
                        }
                        this.auth.loginWithGoogle({ data: JSON.stringify(reqData) })
                            .subscribe(r => {
                                const data = r.datagg;
                                console.log('data', r);
                                if (r['resultCode'] === 'OK') {
                                    localStorage.setItem(USER_KEY, JSON.stringify(r['profile'][0]));
                                    localStorage.setItem(SOCIAL_KEY, JSON.stringify(r['datagg']));
                                    localStorage.setItem(USER_TOKEN, r['token']);
                                    localStorage.setItem(IS_AUTHENTICATED_KEY, 'true');
                                    this.goTabPage();
                                }
                            })
                    })
            })
            .catch(err => {
                console.error(err);
                this.alert.showBasicAlert('login.error_alert.title', 'login.error_alert.sub_title');
            });
    }

    goTabPage() {
        this.navCtrl.setRoot('page-tabs', { tab: 1 });
    }
}
