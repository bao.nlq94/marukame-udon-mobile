import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ListRestaurantPage} from './list-restaurant';
import {ComponentsModule} from "../../components/components.module";
import {BranchProvider} from "../../providers/branch/branch";
import {CookieService} from "ngx-cookie-service";
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {LocationProvider} from "../../providers/location/location";
import { Geolocation } from '@ionic-native/geolocation';
import {LocationAccuracy} from "@ionic-native/location-accuracy";

@NgModule({
    declarations: [
        ListRestaurantPage,
    ],
    imports: [
        IonicPageModule.forChild(ListRestaurantPage),
        ComponentsModule,
        TranslateModule.forChild()
    ],
    providers: [
        BranchProvider,
        CookieService,
        TranslateService,
        LocationProvider,
        Geolocation,
        LocationAccuracy
    ]
})
export class ListRestaurantPageModule {
}
