import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Item } from 'ionic-angular';
import { CookieService } from "ngx-cookie-service";
import { BranchProvider } from "../../providers/branch/branch";
import { BaseResponse } from "../../models/base-response";
import { Branch } from "../../models/branch";
import { LatLng, LocationProvider } from "../../providers/location/location";
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { AuthProvider } from '../../providers/auth/auth';
import { flatten } from '@angular/compiler';

@IonicPage({
    name: 'page-list-restaurant',
    segment: 'page-list-restaurant'
})
@Component({
    selector: 'page-list-restaurant',
    templateUrl: 'list-restaurant.html',
})
export class ListRestaurantPage implements OnInit {
    filtered: string = '1';
    restaurantList: any;

    isLoaded: boolean;
    textSearch: string;
    textSearchTmp: string;
    paging: any;
    listResFavorite: any;
    userId: number;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public appCtrl: App,
        private cookieService: CookieService,
        private branchService: BranchProvider,
        private location: LocationProvider,
        private favoriteProvider: FavoriteProvider,
        private aut: AuthProvider) {

    }

    ngOnInit() {
        this.paging = {
            page: 1,
            size: 2,
            totalPage: 1,
        }
        this.textSearch = "";
        this.textSearchTmp = "";
        this.userId = this.aut.getUser().userId;
        this.favoriteProvider.listFavorite(this.userId).then((res) => {
            this.listResFavorite = res.FavoriteRestaurants;
        });
        this.isLoaded = this.cookieService.check('is_restaurant_list_loaded');
        if (!this.isLoaded)
            this.restaurantList = [];
    }

    ionViewDidLoad() {
        this.loadRestaurantList();
    }

    loadRestaurantList() {
        if (!this.isLoaded) {
            this.branchService.getBranchBusiness()
                .subscribe((res: BaseResponse) => {
                    console.log('loadRestaurantList ', res);
                    if (res.resultCode === 'OK') {
                        this.location.getGeoLocation((resp) => {
                            console.log('geo ', resp);
                            for (let business of res.result) {
                                for (let rest of business.contain) {
                                    rest.distance = this.location.getDistanceFromLatLonInKm(new LatLng(resp.coords.latitude, resp.coords.longitude), new LatLng(rest.blat, rest.blng))
                                }
                                this.restaurantList = this.restaurantList.concat(business.contain);
                            }
                            this.restaurantList.sort((a, b) =>
                                a.distance - b.distance
                            )
                            this.addUserIdAndLike();
                        }, err => console.error(err));
                    }
                });
        }
    }

    private addUserIdAndLike() {
        this.restaurantList.forEach(itemRes => {
            itemRes.userId = this.userId;
            itemRes.isLike = false;
            if (this.listResFavorite) {
                let itemFavorite = this.listResFavorite.find(x => x.bid === itemRes.bid);
                if (itemFavorite) {
                    itemRes.isLike = true;
                } else {
                    itemRes.isLike = false;
                }
            }

        });
        console.log('after add like ', this.restaurantList);
    }

    goFilter(s: string) {
        this.filtered = s;
    }

    goDetailRestaurant(data) {
        this.navCtrl.push('page-detail-restaurant', { info: data });
    }

    goLocation(data) {
        this.navCtrl.push('page-location', { dataBranch: data });
    }

    back() {
        if (this.navCtrl.canGoBack())
            this.navCtrl.pop();
        else
            this.appCtrl.getRootNav().push('page-tabs');
    }

    doInfinite(infiniteScroll) {
        if (this.textSearch === "") {
            infiniteScroll.complete();
        } else {
            if (this.paging.page < this.paging.totalPage) {
                this.search().then((result) => {
                    console.log(result);
                    infiniteScroll.complete();
                })
            } else {
                infiniteScroll.complete();
            }
        }

    }
    
    onSearch(event) {
        this.search().then((result) => {
            console.log(result);
        })
    }

    search(): Promise<boolean> {
        if (this.textSearchTmp !== this.textSearch) {
            this.textSearchTmp = this.textSearch;
            this.restaurantList = [];
            this.paging.page = 1;
            if (this.textSearch === "") {
                this.loadRestaurantList();
                return new Promise<boolean>(resolve => {
                    resolve(true);
                });
            }
        }
        return new Promise<boolean>(resolve => {
            this.location.getGeoLocation((resp) => {
                this.branchService.searchBranch(this.paging.page, this.paging.size,
                    this.textSearch, resp.coords.latitude, resp.coords.longitude).subscribe((res: BaseResponse) => {
                        if (res.resultCode === 'OK') {
                            this.paging.totalPage = res.result.PAGING[0].totalPage;
                            this.paging.page += 1;
                            for (let rest of res.result.CONTAIN) {
                                rest.distance = this.location.getDistanceFromLatLonInKm(new LatLng(resp.coords.latitude, resp.coords.longitude), new LatLng(rest.blat, rest.blng))
                            }
                            this.restaurantList = this.restaurantList.concat(res.result.CONTAIN);
                        }
                        this.addUserIdAndLike();
                        resolve(true);
                    })
            }, err => console.error(err))
        });
    }

}
