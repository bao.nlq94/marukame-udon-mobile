import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  'name': 'page-notification-detail',
  'segment': 'page-notification-detail'
})
@Component({
  selector: 'page-notification-detail',
  templateUrl: 'notification-detail.html',
})
export class NotificationDetailPage {

  info: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams) {
      this.info = this.navParams.get('item');
      //console.log('notification detail ', this.info);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad NotificationDetailPage');
  }

}
