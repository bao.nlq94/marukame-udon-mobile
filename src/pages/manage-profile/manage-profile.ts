import { Component, OnInit } from '@angular/core';
import { LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../models/user';
import { ToastmessageComponent } from '../../components/toastmessage/toastmessage';
import { UserProvider } from '../../providers/user/user';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImagePicker } from "@ionic-native/image-picker";
import { AppSettings, API_ENDPOINT } from "../../app/app.setting";
declare const require;
const moment = require('moment-timezone');
@IonicPage({
    name: 'page-manage-profile',
    segment: 'page-manage-profile'
})
@Component({
    selector: 'page-manage-profile',
    templateUrl: 'manage-profile.html',
})
export class ManageProfilePage implements OnInit {


    profilePicture: string;
    registerForm: FormGroup;
    userId: number;
    model: User;
    maxDate: string = new Date().toISOString();
    fileTransfer: FileTransferObject;
    //fileTransfer: FileTransferObject;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public fb: FormBuilder,
        private translate: TranslateService,
        private loadingCtrl: LoadingController,
        private toastMesssage: ToastmessageComponent,
        private userProfile: UserProvider,
        private transfer: FileTransfer,
        private imagePicker: ImagePicker) {
    }

    ionViewDidLoad() {

    }

    ngOnInit(): void {
        this.model = new User();
        this.fileTransfer = this.transfer.create();
        this.model.gender = 1;
        this.model.dateOfBirth = new Date().toISOString();
        let dataUser = this.navParams.get("dataUser");
        this.buildForm();
        this.getUserProfile(dataUser);
    }

    buildForm(): void {
        this.registerForm = this.fb.group({
            'fullname': [this.model.fullname, [
                Validators.required,
            ]
            ],
            'phone': [this.model.phone, [
                Validators.required,
            ],
            ],
            'email': [this.model.email, [
                Validators.required,
                Validators.email,
            ],
            ],
            'gender': ['1', [],
            ],
            'dateOfBirth': [this.model.dateOfBirth, [],
            ],
            'address': [this.model.address, []],
            'cardno': [this.model.cardno, []]
        });

        this.registerForm.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now
    }
    
    getUserProfile(dataUser: any) {
        //console.log(dataUser);
        this.userId = dataUser.id;
        const birthday = moment.tz(dataUser.dateOfBirth, "Asia/Ho_Chi_Minh");
        this.profilePicture = dataUser.profilePicture;
        this.registerForm.setValue({
            fullname: dataUser.username,
            gender: dataUser.gender,
            dateOfBirth: birthday.toISOString(),
            address: dataUser.address,
            email: dataUser.email,
            phone: dataUser.phoneNumber,
            cardno: dataUser.cardNo
        });
    }

    private getErrorMessage(): string {
        for (const field in this.formErrors) {
            if (this.formErrors[field] != '')
                return this.formErrors[field];
        }
        return "";
    }

    onValueChanged(data?: any) {
        if (!this.registerForm) {
            return;
        }
        const form = this.registerForm;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.translate.get(messages[key]).subscribe(
                        value => {
                            this.formErrors[field] += value + ' ';
                        }
                    )
                }
            }
        }
    }

    onSubmit() {
        const loader = this.loadingCtrl.create({
            content: "Saving..."
        });
        loader.present();
        const data = {
            fullname: this.registerForm.controls['fullname'].value,
            gender: this.registerForm.controls['gender'].value,
            dateOfBirth: this.registerForm.controls['dateOfBirth'].value,
            //"email": this.registerForm.controls['email'].value,
            phoneNumber: this.registerForm.controls['phone'].value,
            address: this.registerForm.controls['address'].value,
            //"profilePic": this.auth.getCurrentUser().profilePicture
        };
        this.userProfile.update(this.userId, data).subscribe(res => {
            loader.dismissAll();
            if (res['resultCode'] === 'OK') {
                //this.auth.updateCurrentUser(res.result[0]);
            }
        }, err => {
            this.toastMesssage.showToast("COMMONS.ERRORSYSTEM");
            loader.dismissAll();
        })
    }

    updateProfilePicture() {
        this.imagePicker.getPictures({ maximumImagesCount: 1 }).then((results) => {
            if (results.length > 0) {
                const imageUri = results[0];
                // console.log('Image URI: ' + imageUri);
                const path = imageUri.split('/');
                const file = path[path.length - 1].split('.');
                const filename = '' + this.userId + '-' + Date.now() + '.' + file[file.length - 1];
                this.fileTransfer.upload(imageUri, API_ENDPOINT + '/user/profile/uploadPicture',
                    {
                        httpMethod: 'POST',
                        params: {
                            'userId': this.userId
                        },
                        fileName: filename
                    })
                    .then((res) => {
                        console.log('data', res);
                        // res = JSON.parse(res);
                        // this.profilePicture = res.user
                        // this.auth.updateProfilePicture(filename, true);
                        //this.getProfile();
                        this.getNewProfile();
                    }, (err) => {
                        // error
                    })
            }
        }, (err) => {
        });
    }

    getNewProfile() {
        this.userProfile.getUserDetail(this.userId).subscribe((res) => {
            if (res.resultCode === "OK") {
                let dataUserProfile = res.result[0];
                this.getUserProfile(dataUserProfile);
            }
        })
    }
    formErrors = {
        'fullname': '',
        'email': '',
        'password': '',
        'phone': ''
    };
    validationMessages = {
        'fullname': {
            'required': 'valid-message.MS0001',
        },
        'email': {
            'required': 'valid-message.MS0002',
        },
        'phone': {
            'required': 'valid-message.MS0003'
        }
    };
}
