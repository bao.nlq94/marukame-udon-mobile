import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageProfilePage } from './manage-profile';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {FileTransfer} from '@ionic-native/file-transfer';
import {ImagePicker} from "@ionic-native/image-picker";
@NgModule({
  declarations: [
    ManageProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ManageProfilePage),
    TranslateModule.forChild(),
  ],
  providers:[
    FileTransfer,
    ImagePicker,
  ]
})
export class ManageProfilePageModule {}
