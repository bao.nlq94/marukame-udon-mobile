import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';

@IonicPage({
    name: 'page-my-profile',
    segment: 'page-my-profile'
})
@Component({
    selector: 'page-my-profile',
    templateUrl: 'my-profile.html',
})
export class MyProfilePage implements OnInit {

    user: any;
    userDetail: any;
    cardScore: any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private authen: AuthProvider,
        private userProvider: UserProvider) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad MyProfilePage');
    }

    ngOnInit(): void {
        this.user = this.authen.getUser();
        this.userDetail = {
            username: '',
            email: '',
            phoneNumber: '',
            profilePicture: '',
        };
        this.cardScore = {
            curentName: '',
            curentLevel: 0,
            curentPoint: 0,
            netxtLevel: 0,
            nextName: '',
            nextPoint: 0,
        }
        this.getUserDetail();
        this.getCardScore();
    }

    getUserDetail() {
        this.userProvider.getUserDetail(this.user.id).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.userDetail = res.result[0];
                console.log('user detail', this.userDetail);
            }
        })
    }

    getCardScore() {
        this.userProvider.getCardScore(this.user.id).subscribe((res) => {
            if (res.resultCode === "OK") {
                this.cardScore.curentName = res.currentLvl.name;
                this.cardScore.curentLevel = res.currentLvl.level;
                this.cardScore.curentPoint = res.currentLvl.point;
                this.cardScore.netxtLevel = res.nextLvl.level;
                this.cardScore.nextName = res.nextLvl.name;
                this.cardScore.nextPoint = res.nextLvl.point;
            }
        })
    }

    gotoManageProfile() {
        this.navCtrl.push("page-manage-profile", { dataUser: this.userDetail })
    }

    myQrCode() {
        // console.log(this.userDetail);
        // console.log('navCtrl', this.navCtrl)
        this.navCtrl.push("page-my-qrcode", { cardNo: this.userDetail.cardNo, dataUser: this.userDetail });
    }

}
