import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewAllReviewPage } from './view-all-review';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    ViewAllReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewAllReviewPage),
    TranslateModule.forChild(),
    ComponentsModule,
  ],
  providers: [
    TranslateService,
  ]
})
export class ViewAllReviewPageModule {}
