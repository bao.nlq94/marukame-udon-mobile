import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BranchProvider } from '../../providers/branch/branch';

/**
 * Generated class for the ViewAllReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'page-viewall-review',
  segment:'page-viewall-review'
})
@Component({
  selector: 'page-view-all-review',
  templateUrl: 'view-all-review.html'
})
export class ViewAllReviewPage  implements OnInit{
 
  reviewList:any;
  bid:number;
  totalComment:number;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private branchProvider: BranchProvider,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAllReviewPage');
  }
  ngOnInit(): void {
    this.bid=this.navParams.get("bid");
    this.totalComment=this.navParams.get("totalComment");
    this.reviewList=[];
    this.getReviewList();
  }

  getReviewList(){
    this.reviewList=[];
    this.branchProvider.getReviewList(this.bid,1,this.totalComment).subscribe((res)=>{
      if(res.resultCode==="OK"){
        console.log(res.result);
        this.reviewList=res.result;
      }
    })
  }

}
